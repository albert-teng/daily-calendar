
var FULL_WIDTH  = 600,
    FULL_HEIGHT = 720,
    DAY_START_TIME = 900;

function convertToHour(timeInMinutes) {
  return (Math.floor(timeInMinutes/60)*100) + (timeInMinutes%60);
}

function addTimeLabels($container) {
  for (var i = 0; i <= FULL_HEIGHT; i+=30) {
    var time = DAY_START_TIME + convertToHour(i) + '',
        period = 'AM',
        minute = time.slice(-2),
        hour   = time.replace(minute, ''),
        $timeLabel = $('<span class="time-label">');

    if ( hour > 12 ) {
      hour -= 12;
      period = 'PM';
    }

    $timeLabel.css("top", i + 'px').text(hour + ":" + minute);
    if ( minute == '00' ) {
      $timeLabel.append($('<span class="period">').text(period));
    } else {
      $timeLabel.addClass('small');
    }

    $("#schedule .label-layer").append($timeLabel);
    $("#schedule .label-layer").append($('<hr>').css("top", i + 'px'));
  }
}

function isOverlappingVertically($newEvent, $existingEvent) {
  var newEventPosition = $newEvent.position(),
      newEventTopBound   = newEventPosition.top,
      newEventLowerBound = newEventTopBound + $newEvent.outerHeight(),
      existingEventPositon = $existingEvent.position(),
      existingEventTopBound   = existingEventPositon.top,
      existingEventLowerBound = existingEventTopBound + $existingEvent.outerHeight();

  if ( (newEventTopBound >= existingEventTopBound && newEventTopBound <= existingEventLowerBound) ||
       (newEventLowerBound >= existingEventTopBound && newEventLowerBound <= existingEventLowerBound) ) {
    return true;
  } else {
    return false;
  }
}

function isOverlappingHorizontally($newEvent, $existingEvent) {
  var newEventPosition = $newEvent.position(),
      newEventLeftBound  = newEventPosition.left,
      newEventRightBound = newEventLeftBound + $newEvent.outerWidth(),
      existingEventPositon = $existingEvent.position(),
      existingEventLeftBound  = existingEventPositon.left,
      existingEventRightBound = existingEventLeftBound + $existingEvent.outerWidth();

  if ( (newEventLeftBound > existingEventLeftBound && newEventLeftBound < existingEventRightBound) ||
       (newEventRightBound >= existingEventLeftBound && newEventRightBound <= existingEventRightBound) ) {
    return true;
  } else {
    return false;
  }
}

function layOutDay(events) {
  var $scheduleContainer = $("#schedule .schedule-layer"),
      overlappingEvents  = [];

  // Set default set of events if nothing is passed in
  if ( typeof events === 'undefined' ) {
    events = [ {start: 30, end: 150}, {start: 540, end: 600}, {start: 560, end: 620}, {start: 610, end: 670} ];
  }

  // Insert events into container
  var startTime = 0,
      duration  = 0;
  for (var index in events) {
    $newEvent = $('<div class="event">');
    startTime = events[index].start;
    duration  = events[index].end - events[index].start;

    // create the event block
    $newEvent.attr("id", "event-"+index).attr("data-overlapwith", "").append('<h3>Sample Item</h3>').append('<p>Sample Location</p>').css({
      top: startTime + 'px',
      height: duration + 'px'
    });

    $scheduleContainer.append($newEvent);
  }

  // Check for overlapping events
  var $scheduledEvents = $scheduleContainer.find(".event");
  $scheduledEvents.each(function(currentEventIndex, element) {
    var $currentEventElement = $(this),
        overlapWith = $currentEventElement.attr("data-overlapwith");

    // Compare current event with existing events to check for overlaps
    $scheduledEvents.each(function (eventIndex, eventElement) {
      var $eventElement = $(this);

      if ( eventIndex != currentEventIndex ) {
        if ( isOverlappingVertically($currentEventElement, $eventElement) ) {
          if ( $eventElement.attr("data-overlapwith") == "" ) {
            $eventElement.attr("data-overlapwith", currentEventIndex);
          } else {
            $eventElement.attr("data-overlapwith", overlapWith+","+currentEventIndex);
          }
        }
      }
    });
  });

  // Adjust overlapping events width
  $scheduledEvents.each(function(index, element) {
    if ( $(this).attr('data-overlapwith') != '' ) {
      var indexArray = $(this).attr('data-overlapwith').split(',');
      var overlapCount = indexArray.length + 1;

      $(this).css("width", (100/overlapCount)+"%");
      for (var i = 0; i < indexArray.length; i++) {
        $nextEvent = $("#event-"+indexArray[i]);
        $nextEvent.css("width", (100/overlapCount)+"%");
      }
    }
  });

  // Adjust events position
  $scheduledEvents.each(function() {
    var $thisEvent = $(this);
    var $nextEvent = $thisEvent.next();

    if ( $nextEvent.length > 0 && $nextEvent.attr('data-overlapwith') != '' ) {
      if ( isOverlappingHorizontally($thisEvent, $nextEvent) ) {
        $nextEvent.css("left", $thisEvent.outerWidth() + "px");
      }
    }
  });
}

$(function() {

  addTimeLabels();
  layOutDay();

}); //-- document.ready
